/**
 * 
 */
package com.myindo.testfuse.processor;

import java.sql.Connection;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author arza
 *
 */
public class CamelProcessor implements Processor {
	Logger log = LoggerFactory.getLogger(CamelProcessor.class);
	Connection conn;

	@Override
	public void process(Exchange exchange) throws Exception {
		 connection();
	}

	private void connection() {

		try {
			PGSimpleDataSource source = new PGSimpleDataSource();
			source.setServerName("postgresql-openshit");
			source.setPortNumber(5432);
			source.setDatabaseName("sampledb");
			source.setUser("mantap");
			source.setPassword("M4ntap");
			conn = source.getConnection();
			log.info("Connection Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.info(e.getMessage());
			log.info("Connection Failed");
			log.info("EXCEPTION PosgresConnection : " + e);
		}

	}

}
